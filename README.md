```bash
Name        : Adelya Gabriel
NPM         : 1906399751
```

## Link Website Story 7 PPW

Story 7 || Deployed at [https://story7ppw-adel.herokuapp.com/](https://story7ppw-adel.herokuapp.com/)

## Status Website

[![pipeline status](https://gitlab.com/adelya.gabriel/story7-ppw/badges/master/pipeline.svg)](https://gitlab.com/adelya.gabriel/story7-ppw/-/commits/master)
[![coverage report](https://gitlab.com/adelya.gabriel/story7-ppw/badges/master/coverage.svg)](https://gitlab.com/adelya.gabriel/story7-ppw/-/commits/master)