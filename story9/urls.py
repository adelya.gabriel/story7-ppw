from django.urls import path
from .views import logIn, signUp, logOut

urlpatterns = [
    path('story9/', logIn, name='login'),
    path('story9/signup/', signUp, name='signup'),
    path('story9/logout/', logOut, name='logout'),
]
