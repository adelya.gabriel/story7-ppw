from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('story8/', views.book ,name='book'),
    path('story8/data/', views.data, name='data'),

]
